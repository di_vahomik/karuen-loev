﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karuen_Loev
{
    public partial class Form1 : Form
    {
        int arraysize;//кол-во отсчетов
        int poryadok_kor; //смещение АКП
        /* ПАРМАТЕРЫ ПОЛИГАРМОНИЧЕСКОГО СИГНАЛА*/
        int num_of_polig_signal; //количество сигнальных состовляющих
        /*double ampl1, ampl2, ampl3;//амплитуды
        double phase1, phase2, phase3;//фазы
        double freq1, fre2, freq3;//частоты*/
        double[] ArrayAmplitudePol; //массив амплитуд
        double[] ArrayPhasePol; //массив фаз
        double[] ArrayFreqPol; //массив частот

        int number_of_sobstv_vector;
        double[] func_poligarmonic;

        /* ПАРМАТЕРЫ ГАУССОВА СИГНАЛА*/
        int num_of_gauss_signal;//количество сигнальных состовляющих
        double[] ArrayAmplitudeGauss; //массив амплитуд
        double[] ArrayCenters; //массив центров
        double[] ArrayDisp; //массив дисперсий

        double[] func_gauss;

        /*ПАРАМЕТРЫ ЭКСПОНЕНЦИАЛЬНО ЗАТУХАЮЩЕГО ПОЛИГАРМОНИЧЕСКОГО СИГНАЛА*/
        int num_of_fading_polig_signal;
        double[] ArrayPhasePolFading; //массив фаз
        double[] ArrayFreqPolFading; //массив частот
        double[] ArrayFadingKoef;

        int n, m;
        double[] func_fading_poligarmonic;
        double nev;
        public Form1()
        {
            InitializeComponent();
            ArrayAmplitudePol = new double[3];
            ArrayPhasePol = new double[3];
            ArrayFreqPol = new double[3];

            ArrayAmplitudeGauss = new double[3];
            ArrayCenters = new double[3];
            ArrayDisp = new double[3];

            ArrayPhasePolFading = new double[3];
            ArrayFreqPolFading = new double[3];
            ArrayFadingKoef = new double[3];

            
        }

        void InitiateModel()
        {
            poryadok_kor = Convert.ToInt32(POR_KOR.Text);

           

            //полигармонический
            ArrayAmplitudePol[0] = Convert.ToDouble(A1.Text);
            ArrayAmplitudePol[1] = Convert.ToDouble(A2.Text);
            ArrayAmplitudePol[2] = Convert.ToDouble(A3.Text);

            ArrayPhasePol[0] = Convert.ToDouble(P1.Text);
            ArrayPhasePol[1] = Convert.ToDouble(P2.Text);
            ArrayPhasePol[2] = Convert.ToDouble(P3.Text);

            ArrayFreqPol[0] = Convert.ToDouble(F1.Text);
            ArrayFreqPol[1] = Convert.ToDouble(F2.Text);
            ArrayFreqPol[2] = Convert.ToDouble(F3.Text);

            //гауссов
            ArrayAmplitudeGauss[0]= Convert.ToDouble(AG1.Text);
            ArrayAmplitudeGauss[1] = Convert.ToDouble(AG2.Text);
            ArrayAmplitudeGauss[2] = Convert.ToDouble(AG3.Text);

            ArrayCenters[0] = Convert.ToDouble(center1.Text);
            ArrayCenters[1] = Convert.ToDouble(center2.Text);
            ArrayCenters[2] = Convert.ToDouble(center3.Text);
            
            ArrayDisp[0] = Convert.ToDouble(disp1.Text);
            ArrayDisp[1] = Convert.ToDouble(disp2.Text);
            ArrayDisp[2] = Convert.ToDouble(disp3.Text);

            //экспоненциально полигармонический сигнал
            ArrayPhasePolFading[0] = Convert.ToDouble(P1Fading.Text);
            ArrayPhasePolFading[1] = Convert.ToDouble(P2Fading.Text);
            ArrayPhasePolFading[2] = Convert.ToDouble(P3Fading.Text);

            ArrayFreqPolFading[0] = Convert.ToDouble(F1Fading.Text);
            ArrayFreqPolFading[1] = Convert.ToDouble(F2Fading.Text);
            ArrayFreqPolFading[2] = Convert.ToDouble(F3Fading.Text);

            ArrayFadingKoef[0] = Convert.ToDouble(FadingKoef1.Text);
            ArrayFadingKoef[1] = Convert.ToDouble(FadingKoef2.Text);
            ArrayFadingKoef[2] = Convert.ToDouble(FadingKoef3.Text);

            num_of_polig_signal = Convert.ToInt32(Number_Of_Polig_Signal.Text);
            num_of_gauss_signal = Convert.ToInt32(Number_Of_Gauss_Signal.Text);
            num_of_fading_polig_signal = Convert.ToInt32(Number_Of_Fading_Polig_Signal.Text);

            arraysize = Convert.ToInt32(ARRAY.Text);
            number_of_sobstv_vector = Convert.ToInt32(NUMBER_OF_SOBCTV_VECTOR.Text);

            func_poligarmonic = new double[arraysize];

            func_gauss = new double[arraysize];


            nev = Convert.ToDouble(NEVYAZKI_TEXTBOX.Text);

            func_fading_poligarmonic = new double[arraysize];
        }

        private double PoligarmFunc( double num, double step, double[] Amp, double[] Frequency, double[] Phas) //функция Гауссово купола для дальнейшей рисовки
        {
            double result = 0;
            for (int i = 0; i < num ; i++)
            {
                result += Amp[i] * Math.Sin(2*Math.PI*Frequency[i]*step + Phas[i]);
            }
            return result;
        }

        private double GaussFunc(double num, double step, double[] Amp, double[] Center, double[] Disp) //функция Гауссово купола для дальнейшей рисовки
        {
            double result = 0;
            for (int i = 0; i < num; i++)
            {
                result += Amp[i] * Math.Exp(-((step - Center[i]) * (step - Center[i])) / (Disp[i] * Disp[i]));
            }
            return result;
        }

        private double FadingPoligarmFunc(double num, double step,  double[] Frequency, double[] Phas, double[] Koef ) //функция Гауссово купола для дальнейшей рисовки
        {
            double result = 0;
            for (int i = 0; i < num; i++)
            {
                result +=  Math.Exp(- Koef[i]*step)*Math.Sin(2*Math.PI*Frequency[i]*step + Phas[i]);
            }
            return result;
        }
       

       private  int svd_hestenes(int m_m, int n_n, double[] a, double[] u, double[] v, double[] sigma)
        {
            double thr = 1e-4f, nul = 1e-16f;
            int n, m, i, j, l, k,  iter, inn, ll, kk;
            bool lort;
            double alfa, betta, hamma, eta, t, cos0, sin0, buf, s;
            n = n_n;
            m = m_m;
            for (i = 0; i < n; i++)
            {
                
                inn = i * n;
                for (j = 0; j < n; j++)
                    if (i == j) v[inn +j] = 1.0;
                    else v[inn +j] = 0.0;
            }
            for (i = 0; i < m; i++)
            {       inn= i * n;
                for (j = 0; j < n; j++)
                {
                    u[inn +j] = a[inn +j];
                }
            }

            iter = 0;
            while (true)
            {
               lort = false;
                iter++;
                for (l = 0; l < n - 1; l++)
                    for (k = l + 1; k < n; k++)
                    {
                        alfa = 0.0; betta = 0.0; hamma = 0.0;
                        for (i = 0; i < m; i++)
                        {
						 inn= i * n;
                            ll =inn+l;
                            kk =inn+k;
                            alfa += u[ll] * u[ll];
                            betta += u[kk] * u[kk];
                            hamma += u[ll] * u[kk];
                        }

                        if (Math.Sqrt(alfa * betta) < nul) continue;
                        if (Math.Abs(hamma) / Math.Sqrt(alfa * betta) < thr) continue;

                        lort = true;
                        eta = (betta - alfa) / (2f * hamma);
                        t = (double)((eta / Math.Abs(eta)) / (Math.Abs(eta) + Math.Sqrt(1.0+ eta * eta)));
                        cos0 = (double)(1.0/ Math.Sqrt(1.0+ t * t));
                        sin0 = t * cos0;

                        for (i = 0; i < m; i++)
                        {
													 inn= i * n;
                            buf = u[inn +l] * cos0 - u[inn +k] * sin0;
                            u[inn +k] = u[inn +l] * sin0 + u[inn +k] * cos0;
                            u[inn +l] = buf;

                            if (i >= n) continue;
                            buf = v[inn +l] * cos0 - v[inn +k] * sin0;
                            v[inn +k] = v[inn +l] * sin0 + v[inn +k] * cos0;
                            v[inn +l] = buf;
                        }
                    }

                if (!lort) break;
            }

            for (i = 0; i < n; i++)
            {
                s = 0.0;
                for (j = 0; j < m; j++) s += u[j * n + i] * u[j * n + i];
                s = (double)Math.Sqrt(s);
                sigma[i] = s;
                if (s < nul) continue;
                for (j = 0; j < m; j++) u[j * n + i] /= s;
            }
            //======= Sortirovka ==============
            for (i = 0; i < n - 1; i++)
                for (j = i; j < n; j++)
                    if (sigma[i] < sigma[j])
                    {
                        s = sigma[i]; sigma[i] = sigma[j]; sigma[j] = s;
                        for (k = 0; k < m; k++)
                        { s = u[i + k * n]; u[i + k * n] = u[j + k * n]; u[j + k * n] = s; }
                        for (k = 0; k < n; k++)
                        { s = v[i + k * n]; v[i + k * n] = v[j + k * n]; v[j + k * n] = s; }
                    }

            return iter;
        }


        private double[] AKM (int poryadok, double num, double [] graphic )
        {
            
            double y;
            double [] kor = new double[(poryadok + 1) * (poryadok + 1)];
           
            for (int i = 0; i < poryadok + 1; i++)
            {
                y = 0;
                for (int j = 0; j < num - i; j++)
                {
                    y += graphic[j] * graphic[i + j];
                }
                kor[i] = y / (num + 1 - i);
            }

            for (int i = 0; i < (poryadok + 1); i++)
            {
                for (int j = 0; j < (poryadok + 1); j++)
                {
                    kor[i * (poryadok + 1) + j] = kor[Math.Abs(j - i)];
                }
            }
            return kor;
           
        }
        private void DRAW_SIGNALS_Click(object sender, EventArgs e)
        {
            InitiateModel();

            Signal_Chart.Series[0].Points.Clear();// сигнал
            Signal_Chart.Series[1].Points.Clear();// сигнал
            Signal_Chart.Series[2].Points.Clear();// сигнал

            Sobctv_Chart.Series[0].Points.Clear();
            Sobctv_Chart.Series[1].Points.Clear();
            Sobctv_Chart.Series[2].Points.Clear();

            SOB_VECTUR_CHART.Series[0].Points.Clear();
            SOB_VECTUR_CHART.Series[1].Points.Clear();
            SOB_VECTUR_CHART.Series[2].Points.Clear();

            

            if (checkBox_poligarm.Checked)
            {

                for (int i = 0; i < arraysize; i++)
                {
                    func_poligarmonic[i] = PoligarmFunc(num_of_polig_signal, i, ArrayAmplitudePol, ArrayFreqPol, ArrayPhasePol);
                }

                for (int i = 1; i < arraysize; i++)
                {
                    Signal_Chart.Series[0].Points.AddXY(i, func_poligarmonic[i]);
                }

                double[] U = new double[(poryadok_kor + 1) * (poryadok_kor + 1)];
                double[] V = new double[(poryadok_kor + 1) * (poryadok_kor + 1)];
                double[] Sigm = new double[(poryadok_kor + 1)* (poryadok_kor + 1)];

                double[] AKM_POLIGARM = new double[(poryadok_kor + 1) * (poryadok_kor + 1)];
                for (int i = 0; i < (poryadok_kor + 1) * (poryadok_kor + 1); i++)
                {
                    U[i] = 0;
                    V[i] = 0;
                    Sigm[i] = 0;
                    AKM_POLIGARM[i] = 0;
                }
                AKM_POLIGARM = AKM(poryadok_kor, arraysize, func_poligarmonic);

                
               svd_hestenes(poryadok_kor + 1, poryadok_kor + 1, AKM_POLIGARM, U, V, Sigm);

               

                for (int i=0; i<poryadok_kor; i++)
                {
                    Sobctv_Chart.Series[0].Points.AddXY(i, Sigm[i]);
                }

              
                double[] V1 = new double[(poryadok_kor + 1) *(poryadok_kor + 1)];
                for (int i = 0; i < (poryadok_kor + 1) * (poryadok_kor + 1); i++)
                {
                    V1[i] = 0;
                }
                for (int i = 0; i < poryadok_kor + 1; i++)
                {
                    if (number_of_sobstv_vector > (poryadok_kor))
                    {
                        MessageBox.Show("Нет такого собственнго вектора");
                        return;
                    }
                    else V1[i] = V[i * (poryadok_kor + 1) + number_of_sobstv_vector];

                   
                }
                for (int i = 0; i < poryadok_kor; i++)
                {
                    SOB_VECTUR_CHART.Series[0].Points.AddXY(i, V1[i]);
                }
               

            }
            else Signal_Chart.Series[0].Points.Clear();// сигнал

            if (checkBox_gauss.Checked)
            { 
                for (int i = 0; i < arraysize; i++)
                {
                    func_gauss[i] = GaussFunc(num_of_gauss_signal, i, ArrayAmplitudeGauss, ArrayCenters, ArrayDisp);
                }

                for (int i = 1; i < arraysize; i++)
                {
                    Signal_Chart.Series[1].Points.AddXY(i, func_gauss[i]);
                }

                double[] AKM_GAUSS = new double[(poryadok_kor + 1) * (poryadok_kor + 1)];

                double[] U = new double[(poryadok_kor + 1) * (poryadok_kor + 1)];
                double[] V = new double[(poryadok_kor + 1) * (poryadok_kor + 1)];
                double[] Sigm = new double[(poryadok_kor + 1) * (poryadok_kor + 1)];

                for(int i=0; i< (poryadok_kor + 1) * (poryadok_kor + 1); i++)
                {
                    U[i]=0;
                    V[i] = 0;
                    Sigm[i] = 0;
                }
                AKM_GAUSS = AKM(poryadok_kor, arraysize, func_gauss);

               

                svd_hestenes(poryadok_kor + 1, poryadok_kor + 1, AKM_GAUSS, U, V, Sigm);
                
                for (int i = 0; i < poryadok_kor; i++)
                {
                    Sobctv_Chart.Series[1].Points.AddXY(i, Sigm[i]);
                }

                double[] V1 = new double[(poryadok_kor + 1) * (poryadok_kor + 1)];
                for (int i = 0; i < (poryadok_kor + 1) * (poryadok_kor + 1); i++)
                {
                    V1[i] = 0;
                }
                for (int i = 0; i < poryadok_kor + 1; i++)
                {
                    if (number_of_sobstv_vector > (poryadok_kor))
                    {
                        MessageBox.Show("Нет такого собственнго вектора");
                        return;
                    }

                    V1[i] = V[i * (poryadok_kor + 1) + number_of_sobstv_vector];

                }
                for (int i = 0; i < poryadok_kor; i++)
                {
                    SOB_VECTUR_CHART.Series[1].Points.AddXY(i, V1[i]);
                }

            }
            else Signal_Chart.Series[1].Points.Clear();// сигнал

            if (checkBox_exp_fading.Checked)
            {

                for (int i = 0; i < arraysize; i++)
                {
                    func_fading_poligarmonic[i] = FadingPoligarmFunc(num_of_fading_polig_signal, i, ArrayFreqPolFading, ArrayPhasePolFading, ArrayFadingKoef);
                }

                for (int i = 1; i < arraysize; i++)
                {
                    Signal_Chart.Series[2].Points.AddXY(i, func_fading_poligarmonic[i]);
                }

                double[] AKM_POLIGARM_FADING = new double[(poryadok_kor + 1) * (poryadok_kor + 1)];

                double[] U = new double[(poryadok_kor + 1) * (poryadok_kor + 1)];
                double[] V = new double[(poryadok_kor + 1) * (poryadok_kor + 1)];
                double[] Sigm = new double[(poryadok_kor + 1)];


                AKM_POLIGARM_FADING = AKM(poryadok_kor, arraysize, func_fading_poligarmonic);

                

                svd_hestenes(poryadok_kor + 1, poryadok_kor + 1, AKM_POLIGARM_FADING, U, V, Sigm);

                for (int i = 0; i < poryadok_kor; i++)
                {
                    Sobctv_Chart.Series[2].Points.AddXY(i, Sigm[i]);
                }
                double[] V1 = new double[(poryadok_kor + 1) * (poryadok_kor + 1)];
                for (int i = 0; i < (poryadok_kor + 1) * (poryadok_kor + 1); i++)
                {
                    V1[i] = 0;
                }
                for (int i = 0; i < poryadok_kor + 1; i++)
                {

                    if (number_of_sobstv_vector > (poryadok_kor))
                    {
                        MessageBox.Show("Нет такого собственнго вектора");
                        return;
                    }

                    V1[i] = V[i * (poryadok_kor + 1) + number_of_sobstv_vector];

                }
                for (int i = 0; i < poryadok_kor; i++)
                {
                    SOB_VECTUR_CHART.Series[2].Points.AddXY(i, V1[i]);
                }

            }
            else Signal_Chart.Series[2].Points.Clear();// сигнал

           
        }

        // Функция для создания матрицы, чтобы каждый раз не писать в новом методе, где это необходимо, принимает размеры n - стр, m -  стлб
     
        private double[,] Ed_Matrix(int n)
        {
            double[,] A = new double [n,n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    A[i,j] = ((i == j) ? 1 : 0);
            return A;
        }
        // Формирование расширенной матрицы R - просто приклеили к А единичную слева, на выводе все видно
        double[,] R_Matrix(int n, double[,] A, double[,] Ed)
        {
            double[,] R = new double[n , 2*n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < 2*n; j++)
                    R[i,j] = ((j < n) ? A[i,j] : Ed[i,j - n]);
                
            }
            return R;
        }
        // Метод, реализующий прямой ход метода Гаусса, формула есть в Фадееве да и в лекциях

         private double[,] Gauss_Tuda(double[,] R, int n, int m)
        {
            double temp=0;

            for (int i = 0; i < n; i++)
                for (int j = i + 1; j < n; j++)
                {
                    temp = (R[i,j] / R[i,i]);
                    for (int k = i; k < m; k++)
                        R[j,k] -= (R[i,k] * temp);

                }
            return R;
        }
        // Метод, находящий детерминант А. В чем суть? - если взять диагональные элементы треугольной матрицы после прямого хода
        //( да, кстати, там получается треугольная матрица),
        // то их произведение равно детерминанту А, это в методичке есть
        double Det(double[,] R, int n)
        {
            double det = 1;
            for (int i = 0; i < n; i++)
                det *= R[i,i];
            return det;
        }


      
        private void RESULT_BUTTON_Click(object sender, EventArgs e)
        {
            
            double[,] mass;
            if (CHAST_check.Checked)
            {
                double[] mass_1 = new double[9] { 1.01, 2.01, 3.01, 4.01, 5.01, 6.01, 7.01, 8.01, 9.01 };
                mass =new double[3, 3] { { 1.01, 2.01, 3.01 }, { 4.01, 5.01, 6.01 }, { 7.01, 8.01, 9.01 } };
                int[,] triangle = new int[3, 3];

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        triangle[i, j] = (int)mass[i, j];
                    }
                }
                double[,] rev = new double[3, 3];
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        rev[i, j] = mass[i, j];
                    }
                }

                
                double[,] Pryamoy = Gauss_Tuda(R_Matrix(3, rev, Ed_Matrix(3)), 3, 6);
                

                N_text.Text = Convert.ToString(3);
                M_text.Text = Convert.ToString(3);

                double[] U = new double[9];
                double[] V = new double[9];
                double[] Sigma = new double[9];
                svd_hestenes(3, 3, mass_1, U, V, Sigma);
                double[,] V_matr = new double[3, 3];
                double[,] U_matr_trans = new double[3, 3];
                double[,] U_matr = new double[3, 3];
                double[,] Sigm_matr = new double[3, 3];
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        V_matr[i, j] = V[i * 3 + j];

                    }
                }

                double det=Det(Pryamoy,3);

                
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        U_matr[i, j] = U[i * 3 + j];

                    }

                }
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        U_matr_trans[i, j] = U_matr[j, i];
                    }
                }



                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (i == j)
                        {
                            if (Sigma[i] >= 0.1 * Sigma[0])
                            {
                                Sigm_matr[i, j] = 1 / Sigma[i];
                            }
                            else Sigm_matr[i, j] = 0;
                        }

                        else Sigm_matr[i, j] = 0;
                    }
                }
                double[,] Multiply_1 = new double[3, 3];
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        for (int k = 0; k < 3; k++)
                        {
                            Multiply_1[i, j] += V_matr[i, k] * Sigm_matr[k, j];
                        }
                    }
                }
                double[,] Psevdo_obratnaya = new double[3, 3];
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        for (int k = 0; k < 3; k++)
                        {
                            Psevdo_obratnaya[i, j] += Multiply_1[i, k] * U_matr_trans[k, j];
                        }
                    }
                }
                Random rnd = new Random();
                double[] mass_b = new double[3];

                for (int i = 0; i < 3; i++)
                {
                    mass_b[i] = Convert.ToDouble(rnd.Next(-2000, 2000)) / 2000;
                }

                double[] Sol = new double[3];
                double[] Psev = new double[3 * 3];
                int z = 0;
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        Psev[z] = Psevdo_obratnaya[i, j];
                        z++;
                    }
                }

                for (int i = 0; i < 3; i++)
                {
                    Sol[i] = 0.0;

                    for (int j = 0; j < 3; j++)
                    {
                        Sol[i] += Psev[i * 3 + j] * mass_b[j];
                    }


                }

                // Вычисление невязки:
                Double Err = 0.0;
                for (int i = 0; i < 3; i++)
                {
                    Double Mult = 0.0;
                    for (int j = 0; j < 3; j++)
                    {
                        Mult += mass_1[i * 3 + j] * Sol[j];
                    }

                    Err += (Mult - mass_b[i]) * (Mult - mass_b[i]) / 3;
                }

                NEVYAZKI_TEXTBOX.Text = Convert.ToString(String.Format("{0:F4}", Err));

                StringBuilder sw = new StringBuilder();

                sw.AppendLine("Исходный массив:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Append(mass[i, j] + "   " );
                    }
                    sw.AppendLine("\n");
                }
                sw.AppendLine("\n");

                sw.AppendLine("Детерминант:" + " " + Math.Round(det, 3) + " ");
               
                sw.AppendLine("\n");
                sw.AppendLine("Матрица правых собственных векторов:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Append(Math.Round(V_matr[i, j], 3) + "   ");
                    }
                    sw.AppendLine("\n");
                }
                sw.AppendLine("\n");
                sw.AppendLine("Матрица левых собственных векторов:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Append(Math.Round(U_matr[i, j],3 ) + "   ");
                    }
                    sw.AppendLine("\n");
                }
                sw.AppendLine("\n");
                sw.AppendLine("Транспонированная матрица левых собственных векторов:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Append(Math.Round(U_matr_trans[i, j], 3) + "   ");
                    }
                    sw.AppendLine("\n");
                }
                sw.AppendLine("\n");
                sw.AppendLine("Матрица собственных значений возведенных с степень -1:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Append(Math.Round(Sigm_matr[i, j], 3) + "   ");
                    }
                    sw.AppendLine("\n" + "   ");
                }
                sw.AppendLine("\n");
                sw.AppendLine("Псевдообратная матрица:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        sw.Append(Math.Round(Psevdo_obratnaya[i, j], 3) + "   ");
                    }
                    sw.AppendLine("\n");
                }
                sw.AppendLine("\n");
                sw.AppendLine("Вектор свободных членов:");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {

                    sw.AppendLine(Math.Round(mass_b[i], 4) + "\n");

                }
                sw.AppendLine("\n");
                sw.AppendLine("Вектор неизвестных:" + "\n" + "\n");
                sw.AppendLine("\n");
                for (int i = 0; i < 3; i++)
                {

                    sw.AppendLine(Math.Round(Sol[i], 4) + "\n");

                }
                sw.AppendLine("\n");

                RESULT.TextAlign = HorizontalAlignment.Center;
                RESULT.Text = sw.ToString();


            }
            else 
            {
                
                n = Convert.ToInt32(N_text.Text);
                m = Convert.ToInt32(M_text.Text);

                if (n == m)
                {


                    

                    mass = new double[n, m];
                    Random rnd = new Random();
                    double[] mass_1 = new double[n * m];
                    for (int i = 0; i < n * m; i++)
                    {
                        mass_1[i] = Convert.ToDouble(rnd.Next(-2000, 2000)) / 2000;
                    }

                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            mass[i, j] = mass_1[i * n + j];

                        }

                    }

                    int[,] triangle = new int[n, m];

                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            triangle[i, j] = (int)mass[i, j];
                        }
                    }
                   

                    double[,] rev = new double[n, m];
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            rev[i, j] = mass[i, j];
                        }
                    }


                    
                    double[,] Pryamoy = Gauss_Tuda(R_Matrix(n, rev, Ed_Matrix(n)), n, 2 * m);

                   
                    double[] U = new double[n * m];
                    double[] V = new double[n * m];
                    double[] Sigma = new double[n * m];

                    double det = Det(Pryamoy, n);

                    svd_hestenes(n, m, mass_1, U, V, Sigma);
                    double[,] V_matr = new double[n, m];
                    double[,] U_matr_trans = new double[n, m];
                    double[,] U_matr = new double[n, m];
                    double[,] Sigm_matr = new double[n, m];
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            V_matr[i, j] = V[i * 3 + j];

                        }
                    }


                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            U_matr[i, j] = U[i * 3 + j];

                        }

                    }
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            U_matr_trans[i, j] = U_matr[j, i];
                        }
                    }



                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            if (i == j)
                            {
                                if (Sigma[i] >= 0.1 * Sigma[0])
                                {
                                    Sigm_matr[i, j] = 1 / Sigma[i];
                                }
                                else Sigm_matr[i, j] = 0;
                            }

                            else Sigm_matr[i, j] = 0;
                        }
                    }
                    double[,] Multiply_1 = new double[n, m];
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            for (int k = 0; k < n; k++)
                            {
                                Multiply_1[i, j] += V_matr[i, k] * Sigm_matr[k, j];
                            }
                        }
                    }
                    double[,] Psevdo_obratnaya = new double[n, m];
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            for (int k = 0; k < n; k++)
                            {
                                Psevdo_obratnaya[i, j] += Multiply_1[i, k] * U_matr_trans[k, j];
                            }
                        }
                    }

                    double[] mass_b = new double[n];

                    for (int i = 0; i < n; i++)
                    {
                        mass_b[i] = Convert.ToDouble(rnd.Next(-2000, 2000)) / 2000;
                    }

                    double[] Sol = new double[n];
                    double[] Psev = new double[n * m];
                    int z = 0;
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            Psev[z] = Psevdo_obratnaya[i, j];
                            z++;
                        }
                    }

                    for (int i = 0; i < n; i++)
                    {
                        Sol[i] = 0.0;

                        for (int j = 0; j < n; j++)
                        {
                            Sol[i] += Psev[i * n + j] * mass_b[j];
                        }


                    }

                    // Вычисление невязки:
                    Double Err = 0.0;
                    for (int i = 0; i < n; i++)
                    {
                        Double Mult = 0.0;
                        for (int j = 0; j < n; j++)
                        {
                            Mult += mass_1[i * n + j] * Sol[j];
                        }

                        Err += (Mult - mass_b[i]) * (Mult - mass_b[i]) / n;
                    }

                    NEVYAZKI_TEXTBOX.Text = Convert.ToString(String.Format("{0:F4}", Err));

                    StringBuilder sw = new StringBuilder();

                    sw.AppendLine("Исходный массив:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            sw.Append(Math.Round(mass[i, j],2) + "   ");
                        }
                        sw.AppendLine("\n");
                    }
                    sw.AppendLine("\n");

                    sw.AppendLine("Детерминант:" + " " + Math.Round(det, 3) + "   ");
                
                    sw.AppendLine("\n");
                    sw.AppendLine("Матрица правых собственных векторов:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                     {
                         for (int j = 0; j < m; j++)
                         {
                            sw.Append(Math.Round(V_matr[i, j], 3) + "   ");
                         }
                        sw.AppendLine("\n");
                     }
                     sw.AppendLine("\n");
                    sw.AppendLine("Матрица левых собственных векторов:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                     {
                         for (int j = 0; j < m; j++)
                         {
                            sw.Append(Math.Round(U_matr[i, j], 3) + "   ");
                         }
                        sw.AppendLine("\n");
                     }
                    sw.AppendLine("\n");
                    sw.AppendLine("Транспонированная матрица левых собственных векторов:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                     {
                         for (int j = 0; j < m; j++)
                         {
                            sw.Append(Math.Round(U_matr_trans[i, j], 3) + "   ");
                         }
                        sw.AppendLine("\n");
                     }
                    sw.AppendLine("\n");
                    sw.AppendLine("Матрица собственных значений возведенных с степень -1:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                     {
                         for (int j = 0; j < m; j++)
                         {
                            sw.Append(Math.Round(Sigm_matr[i, j], 3) + "   ");
                         }
                        sw.AppendLine("\n");
                     }
                    sw.AppendLine("\n");
                    sw.AppendLine("Псевдообратная матрица:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                     {
                         for (int j = 0; j < m; j++)
                         {
                            sw.Append(Math.Round(Psevdo_obratnaya[i, j], 3) + "   ");
                         }
                        sw.AppendLine("\n");
                     }
                    sw.AppendLine("\n");
                    sw.AppendLine("Вектор свободных членов:");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                     {

                        sw.AppendLine(Math.Round(mass_b[i], 4) + "\n");

                     }
                    sw.AppendLine("\n");
                    sw.AppendLine("Вектор неизвестных:" + "\n" + "\n");
                    sw.AppendLine("\n");
                    for (int i = 0; i < n; i++)
                     {

                        sw.AppendLine(Math.Round(Sol[i], 4) + "\n");

                     }
                    sw.AppendLine("\n");
                   
                    RESULT.TextAlign = HorizontalAlignment.Center;
                    RESULT.Text = sw.ToString();
                }
                else
                {
                    MessageBox.Show("Выберите другие значения N и M");
                    return;
                }
            }


        }
    }
}
