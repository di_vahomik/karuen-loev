﻿namespace Karuen_Loev
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Signal_Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.ARRAY = new System.Windows.Forms.TextBox();
            this.Number_Of_Polig_Signal = new System.Windows.Forms.TextBox();
            this.A1 = new System.Windows.Forms.TextBox();
            this.F1 = new System.Windows.Forms.TextBox();
            this.P1 = new System.Windows.Forms.TextBox();
            this.A2 = new System.Windows.Forms.TextBox();
            this.F2 = new System.Windows.Forms.TextBox();
            this.P2 = new System.Windows.Forms.TextBox();
            this.A3 = new System.Windows.Forms.TextBox();
            this.F3 = new System.Windows.Forms.TextBox();
            this.P3 = new System.Windows.Forms.TextBox();
            this.DRAW_SIGNALS = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Number_Of_Gauss_Signal = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.disp3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.center1 = new System.Windows.Forms.TextBox();
            this.disp2 = new System.Windows.Forms.TextBox();
            this.center2 = new System.Windows.Forms.TextBox();
            this.AG3 = new System.Windows.Forms.TextBox();
            this.AG1 = new System.Windows.Forms.TextBox();
            this.center3 = new System.Windows.Forms.TextBox();
            this.disp1 = new System.Windows.Forms.TextBox();
            this.AG2 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.FadingKoef3 = new System.Windows.Forms.TextBox();
            this.FadingKoef2 = new System.Windows.Forms.TextBox();
            this.FadingKoef1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Number_Of_Fading_Polig_Signal = new System.Windows.Forms.TextBox();
            this.F2Fading = new System.Windows.Forms.TextBox();
            this.F1Fading = new System.Windows.Forms.TextBox();
            this.P3Fading = new System.Windows.Forms.TextBox();
            this.P1Fading = new System.Windows.Forms.TextBox();
            this.F3Fading = new System.Windows.Forms.TextBox();
            this.P2Fading = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.POR_KOR = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.checkBox_gauss = new System.Windows.Forms.CheckBox();
            this.checkBox_exp_fading = new System.Windows.Forms.CheckBox();
            this.checkBox_poligarm = new System.Windows.Forms.CheckBox();
            this.Sobctv_Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.SOB_VECTUR_CHART = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.NUMBER_OF_SOBCTV_VECTOR = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.RESULT = new System.Windows.Forms.TextBox();
            this.RESULT_BUTTON = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.NEVYAZKI_TEXTBOX = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.M_text = new System.Windows.Forms.TextBox();
            this.N_text = new System.Windows.Forms.TextBox();
            this.CHAST_check = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.Signal_Chart)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Sobctv_Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SOB_VECTUR_CHART)).BeginInit();
            this.SuspendLayout();
            // 
            // Signal_Chart
            // 
            chartArea4.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea4.AxisX.Interval = 100D;
            chartArea4.Name = "ChartArea1";
            this.Signal_Chart.ChartAreas.Add(chartArea4);
            legend4.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Row;
            legend4.Name = "Legend1";
            legend4.Position.Auto = false;
            legend4.Position.Height = 8.979591F;
            legend4.Position.Width = 79.92307F;
            legend4.Position.X = 20.07693F;
            legend4.Position.Y = 90F;
            this.Signal_Chart.Legends.Add(legend4);
            this.Signal_Chart.Location = new System.Drawing.Point(12, 34);
            this.Signal_Chart.Name = "Signal_Chart";
            series10.BorderWidth = 2;
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series10.Color = System.Drawing.Color.DarkTurquoise;
            series10.Legend = "Legend1";
            series10.LegendText = "Полигармонический";
            series10.Name = "Series1";
            series11.BorderWidth = 2;
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series11.Color = System.Drawing.Color.Red;
            series11.Legend = "Legend1";
            series11.LegendText = "Гауссов";
            series11.Name = "Series2";
            series12.BorderWidth = 2;
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series12.Color = System.Drawing.Color.Purple;
            series12.Legend = "Legend1";
            series12.LegendText = "Экспоненциально-затухающий";
            series12.Name = "Series3";
            this.Signal_Chart.Series.Add(series10);
            this.Signal_Chart.Series.Add(series11);
            this.Signal_Chart.Series.Add(series12);
            this.Signal_Chart.Size = new System.Drawing.Size(540, 248);
            this.Signal_Chart.TabIndex = 0;
            this.Signal_Chart.Text = "chart1";
            // 
            // ARRAY
            // 
            this.ARRAY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.ARRAY.Location = new System.Drawing.Point(705, 214);
            this.ARRAY.Name = "ARRAY";
            this.ARRAY.Size = new System.Drawing.Size(54, 21);
            this.ARRAY.TabIndex = 1;
            this.ARRAY.Text = "600";
            // 
            // Number_Of_Polig_Signal
            // 
            this.Number_Of_Polig_Signal.Location = new System.Drawing.Point(116, 29);
            this.Number_Of_Polig_Signal.Name = "Number_Of_Polig_Signal";
            this.Number_Of_Polig_Signal.Size = new System.Drawing.Size(71, 21);
            this.Number_Of_Polig_Signal.TabIndex = 2;
            this.Number_Of_Polig_Signal.Text = "3";
            // 
            // A1
            // 
            this.A1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.A1.Location = new System.Drawing.Point(89, 58);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(37, 23);
            this.A1.TabIndex = 3;
            this.A1.Text = "1";
            // 
            // F1
            // 
            this.F1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.F1.Location = new System.Drawing.Point(89, 87);
            this.F1.Name = "F1";
            this.F1.Size = new System.Drawing.Size(37, 23);
            this.F1.TabIndex = 4;
            this.F1.Text = "2e-2";
            // 
            // P1
            // 
            this.P1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.P1.Location = new System.Drawing.Point(89, 116);
            this.P1.Name = "P1";
            this.P1.Size = new System.Drawing.Size(37, 23);
            this.P1.TabIndex = 5;
            this.P1.Text = "5e-1";
            // 
            // A2
            // 
            this.A2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.A2.Location = new System.Drawing.Point(132, 58);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(37, 23);
            this.A2.TabIndex = 6;
            this.A2.Text = "2";
            // 
            // F2
            // 
            this.F2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.F2.Location = new System.Drawing.Point(132, 87);
            this.F2.Name = "F2";
            this.F2.Size = new System.Drawing.Size(37, 23);
            this.F2.TabIndex = 7;
            this.F2.Text = "15e-3";
            // 
            // P2
            // 
            this.P2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.P2.Location = new System.Drawing.Point(132, 116);
            this.P2.Name = "P2";
            this.P2.Size = new System.Drawing.Size(37, 23);
            this.P2.TabIndex = 8;
            this.P2.Text = "5e-1";
            // 
            // A3
            // 
            this.A3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.A3.Location = new System.Drawing.Point(175, 58);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(37, 23);
            this.A3.TabIndex = 9;
            this.A3.Text = "1";
            // 
            // F3
            // 
            this.F3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.F3.Location = new System.Drawing.Point(175, 87);
            this.F3.Name = "F3";
            this.F3.Size = new System.Drawing.Size(37, 23);
            this.F3.TabIndex = 10;
            this.F3.Text = "1e-2";
            // 
            // P3
            // 
            this.P3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.P3.Location = new System.Drawing.Point(175, 116);
            this.P3.Name = "P3";
            this.P3.Size = new System.Drawing.Size(37, 23);
            this.P3.TabIndex = 11;
            this.P3.Text = "6e-1";
            // 
            // DRAW_SIGNALS
            // 
            this.DRAW_SIGNALS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.DRAW_SIGNALS.Location = new System.Drawing.Point(784, 217);
            this.DRAW_SIGNALS.Name = "DRAW_SIGNALS";
            this.DRAW_SIGNALS.Size = new System.Drawing.Size(165, 40);
            this.DRAW_SIGNALS.TabIndex = 12;
            this.DRAW_SIGNALS.Text = "Нарисовать сигналы";
            this.DRAW_SIGNALS.UseVisualStyleBackColor = true;
            this.DRAW_SIGNALS.Click += new System.EventHandler(this.DRAW_SIGNALS_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.Number_Of_Polig_Signal);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.F2);
            this.groupBox1.Controls.Add(this.A1);
            this.groupBox1.Controls.Add(this.F1);
            this.groupBox1.Controls.Add(this.P3);
            this.groupBox1.Controls.Add(this.P1);
            this.groupBox1.Controls.Add(this.F3);
            this.groupBox1.Controls.Add(this.A2);
            this.groupBox1.Controls.Add(this.A3);
            this.groupBox1.Controls.Add(this.P2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox1.Location = new System.Drawing.Point(581, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(226, 159);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Полигармонический сигнал";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label3.Location = new System.Drawing.Point(10, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Фаза :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label4.Location = new System.Drawing.Point(10, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 17;
            this.label4.Text = "Кол-во :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label2.Location = new System.Drawing.Point(10, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 15;
            this.label2.Text = "Частоты :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.Location = new System.Drawing.Point(10, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 15);
            this.label1.TabIndex = 14;
            this.label1.Text = "Амплитуды :";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.MistyRose;
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.Number_Of_Gauss_Signal);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.disp3);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.center1);
            this.groupBox2.Controls.Add(this.disp2);
            this.groupBox2.Controls.Add(this.center2);
            this.groupBox2.Controls.Add(this.AG3);
            this.groupBox2.Controls.Add(this.AG1);
            this.groupBox2.Controls.Add(this.center3);
            this.groupBox2.Controls.Add(this.disp1);
            this.groupBox2.Controls.Add(this.AG2);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox2.Location = new System.Drawing.Point(814, 39);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(244, 159);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Гауссов сигнал ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label8.Location = new System.Drawing.Point(5, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 15);
            this.label8.TabIndex = 20;
            this.label8.Text = "Кол-во :";
            // 
            // Number_Of_Gauss_Signal
            // 
            this.Number_Of_Gauss_Signal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Number_Of_Gauss_Signal.Location = new System.Drawing.Point(137, 29);
            this.Number_Of_Gauss_Signal.Name = "Number_Of_Gauss_Signal";
            this.Number_Of_Gauss_Signal.Size = new System.Drawing.Size(71, 23);
            this.Number_Of_Gauss_Signal.TabIndex = 20;
            this.Number_Of_Gauss_Signal.Text = "3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 121);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 15);
            this.label5.TabIndex = 7;
            this.label5.Text = "Дисперсии:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 91);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "Центры куполов:";
            // 
            // disp3
            // 
            this.disp3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.disp3.Location = new System.Drawing.Point(195, 120);
            this.disp3.Margin = new System.Windows.Forms.Padding(2);
            this.disp3.Name = "disp3";
            this.disp3.Size = new System.Drawing.Size(39, 23);
            this.disp3.TabIndex = 15;
            this.disp3.Text = "5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 64);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 15);
            this.label7.TabIndex = 5;
            this.label7.Text = "Амплитуды:";
            // 
            // center1
            // 
            this.center1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.center1.Location = new System.Drawing.Point(111, 90);
            this.center1.Margin = new System.Windows.Forms.Padding(2);
            this.center1.Name = "center1";
            this.center1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.center1.Size = new System.Drawing.Size(37, 23);
            this.center1.TabIndex = 8;
            this.center1.Text = "150";
            // 
            // disp2
            // 
            this.disp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.disp2.Location = new System.Drawing.Point(154, 120);
            this.disp2.Margin = new System.Windows.Forms.Padding(2);
            this.disp2.Name = "disp2";
            this.disp2.Size = new System.Drawing.Size(37, 23);
            this.disp2.TabIndex = 14;
            this.disp2.Text = "9";
            // 
            // center2
            // 
            this.center2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.center2.Location = new System.Drawing.Point(154, 90);
            this.center2.Margin = new System.Windows.Forms.Padding(2);
            this.center2.Name = "center2";
            this.center2.Size = new System.Drawing.Size(37, 23);
            this.center2.TabIndex = 9;
            this.center2.Text = "302";
            // 
            // AG3
            // 
            this.AG3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.AG3.Location = new System.Drawing.Point(195, 60);
            this.AG3.Margin = new System.Windows.Forms.Padding(2);
            this.AG3.Name = "AG3";
            this.AG3.Size = new System.Drawing.Size(39, 23);
            this.AG3.TabIndex = 2;
            this.AG3.Text = "4";
            // 
            // AG1
            // 
            this.AG1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.AG1.Location = new System.Drawing.Point(111, 61);
            this.AG1.Margin = new System.Windows.Forms.Padding(2);
            this.AG1.Name = "AG1";
            this.AG1.Size = new System.Drawing.Size(37, 23);
            this.AG1.TabIndex = 0;
            this.AG1.Text = "5";
            // 
            // center3
            // 
            this.center3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.center3.Location = new System.Drawing.Point(195, 90);
            this.center3.Margin = new System.Windows.Forms.Padding(2);
            this.center3.Name = "center3";
            this.center3.Size = new System.Drawing.Size(39, 23);
            this.center3.TabIndex = 10;
            this.center3.Text = "456";
            // 
            // disp1
            // 
            this.disp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.disp1.Location = new System.Drawing.Point(111, 120);
            this.disp1.Margin = new System.Windows.Forms.Padding(2);
            this.disp1.Name = "disp1";
            this.disp1.Size = new System.Drawing.Size(37, 23);
            this.disp1.TabIndex = 13;
            this.disp1.Text = "6";
            // 
            // AG2
            // 
            this.AG2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.AG2.Location = new System.Drawing.Point(154, 61);
            this.AG2.Margin = new System.Windows.Forms.Padding(2);
            this.AG2.Name = "AG2";
            this.AG2.Size = new System.Drawing.Size(37, 23);
            this.AG2.TabIndex = 1;
            this.AG2.Text = "6";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Info;
            this.groupBox3.Controls.Add(this.FadingKoef3);
            this.groupBox3.Controls.Add(this.FadingKoef2);
            this.groupBox3.Controls.Add(this.FadingKoef1);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.Number_Of_Fading_Polig_Signal);
            this.groupBox3.Controls.Add(this.F2Fading);
            this.groupBox3.Controls.Add(this.F1Fading);
            this.groupBox3.Controls.Add(this.P3Fading);
            this.groupBox3.Controls.Add(this.P1Fading);
            this.groupBox3.Controls.Add(this.F3Fading);
            this.groupBox3.Controls.Add(this.P2Fading);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox3.Location = new System.Drawing.Point(1064, 39);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(251, 159);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Полигармонический затухающий сигнал";
            // 
            // FadingKoef3
            // 
            this.FadingKoef3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FadingKoef3.Location = new System.Drawing.Point(203, 128);
            this.FadingKoef3.Name = "FadingKoef3";
            this.FadingKoef3.Size = new System.Drawing.Size(37, 23);
            this.FadingKoef3.TabIndex = 22;
            this.FadingKoef3.Text = "2e-2";
            // 
            // FadingKoef2
            // 
            this.FadingKoef2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FadingKoef2.Location = new System.Drawing.Point(160, 128);
            this.FadingKoef2.Name = "FadingKoef2";
            this.FadingKoef2.Size = new System.Drawing.Size(37, 23);
            this.FadingKoef2.TabIndex = 23;
            this.FadingKoef2.Text = "3e-2";
            // 
            // FadingKoef1
            // 
            this.FadingKoef1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FadingKoef1.Location = new System.Drawing.Point(116, 128);
            this.FadingKoef1.Name = "FadingKoef1";
            this.FadingKoef1.Size = new System.Drawing.Size(37, 23);
            this.FadingKoef1.TabIndex = 24;
            this.FadingKoef1.Text = "1e-2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 134);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(110, 15);
            this.label14.TabIndex = 18;
            this.label14.Text = "Коэф. затухания :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label9.Location = new System.Drawing.Point(6, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 15);
            this.label9.TabIndex = 16;
            this.label9.Text = "Фаза :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label10.Location = new System.Drawing.Point(6, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 15);
            this.label10.TabIndex = 17;
            this.label10.Text = "Кол-во :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label11.Location = new System.Drawing.Point(6, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 15);
            this.label11.TabIndex = 15;
            this.label11.Text = "Частоты :";
            // 
            // Number_Of_Fading_Polig_Signal
            // 
            this.Number_Of_Fading_Polig_Signal.Location = new System.Drawing.Point(143, 42);
            this.Number_Of_Fading_Polig_Signal.Name = "Number_Of_Fading_Polig_Signal";
            this.Number_Of_Fading_Polig_Signal.Size = new System.Drawing.Size(71, 21);
            this.Number_Of_Fading_Polig_Signal.TabIndex = 2;
            this.Number_Of_Fading_Polig_Signal.Text = "3";
            // 
            // F2Fading
            // 
            this.F2Fading.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.F2Fading.Location = new System.Drawing.Point(159, 71);
            this.F2Fading.Name = "F2Fading";
            this.F2Fading.Size = new System.Drawing.Size(37, 23);
            this.F2Fading.TabIndex = 7;
            this.F2Fading.Text = "15e-3";
            // 
            // F1Fading
            // 
            this.F1Fading.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.F1Fading.Location = new System.Drawing.Point(116, 71);
            this.F1Fading.Name = "F1Fading";
            this.F1Fading.Size = new System.Drawing.Size(37, 23);
            this.F1Fading.TabIndex = 4;
            this.F1Fading.Text = "2e-2";
            // 
            // P3Fading
            // 
            this.P3Fading.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.P3Fading.Location = new System.Drawing.Point(202, 100);
            this.P3Fading.Name = "P3Fading";
            this.P3Fading.Size = new System.Drawing.Size(37, 23);
            this.P3Fading.TabIndex = 11;
            this.P3Fading.Text = "6e-1";
            // 
            // P1Fading
            // 
            this.P1Fading.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.P1Fading.Location = new System.Drawing.Point(116, 100);
            this.P1Fading.Name = "P1Fading";
            this.P1Fading.Size = new System.Drawing.Size(37, 23);
            this.P1Fading.TabIndex = 5;
            this.P1Fading.Text = "5e-1";
            // 
            // F3Fading
            // 
            this.F3Fading.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.F3Fading.Location = new System.Drawing.Point(202, 71);
            this.F3Fading.Name = "F3Fading";
            this.F3Fading.Size = new System.Drawing.Size(37, 23);
            this.F3Fading.TabIndex = 10;
            this.F3Fading.Text = "1e-2";
            // 
            // P2Fading
            // 
            this.P2Fading.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.P2Fading.Location = new System.Drawing.Point(159, 100);
            this.P2Fading.Name = "P2Fading";
            this.P2Fading.Size = new System.Drawing.Size(37, 23);
            this.P2Fading.TabIndex = 8;
            this.P2Fading.Text = "5e-1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label13.Location = new System.Drawing.Point(578, 217);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 15);
            this.label13.TabIndex = 21;
            this.label13.Text = "Кол-во отсчетов:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(217, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(145, 17);
            this.label12.TabIndex = 22;
            this.label12.Text = "Графики сигналов";
            // 
            // POR_KOR
            // 
            this.POR_KOR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.POR_KOR.Location = new System.Drawing.Point(705, 246);
            this.POR_KOR.Name = "POR_KOR";
            this.POR_KOR.Size = new System.Drawing.Size(54, 21);
            this.POR_KOR.TabIndex = 23;
            this.POR_KOR.Text = "60";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label15.Location = new System.Drawing.Point(578, 246);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 15);
            this.label15.TabIndex = 24;
            this.label15.Text = "Порядок";
            // 
            // checkBox_gauss
            // 
            this.checkBox_gauss.AutoSize = true;
            this.checkBox_gauss.Location = new System.Drawing.Point(857, 16);
            this.checkBox_gauss.Name = "checkBox_gauss";
            this.checkBox_gauss.Size = new System.Drawing.Size(105, 17);
            this.checkBox_gauss.TabIndex = 25;
            this.checkBox_gauss.Text = "Гауссов сигнал";
            this.checkBox_gauss.UseVisualStyleBackColor = true;
            // 
            // checkBox_exp_fading
            // 
            this.checkBox_exp_fading.AutoSize = true;
            this.checkBox_exp_fading.Location = new System.Drawing.Point(1093, 16);
            this.checkBox_exp_fading.Name = "checkBox_exp_fading";
            this.checkBox_exp_fading.Size = new System.Drawing.Size(185, 17);
            this.checkBox_exp_fading.TabIndex = 26;
            this.checkBox_exp_fading.Text = "Экспоненциально- затухающий";
            this.checkBox_exp_fading.UseVisualStyleBackColor = true;
            // 
            // checkBox_poligarm
            // 
            this.checkBox_poligarm.AutoSize = true;
            this.checkBox_poligarm.Location = new System.Drawing.Point(600, 16);
            this.checkBox_poligarm.Name = "checkBox_poligarm";
            this.checkBox_poligarm.Size = new System.Drawing.Size(168, 17);
            this.checkBox_poligarm.TabIndex = 27;
            this.checkBox_poligarm.Text = "Полигармонический сигнал";
            this.checkBox_poligarm.UseVisualStyleBackColor = true;
            // 
            // Sobctv_Chart
            // 
            chartArea5.AxisX.Interval = 10D;
            chartArea5.AxisX.Minimum = 0D;
            chartArea5.Name = "ChartArea1";
            this.Sobctv_Chart.ChartAreas.Add(chartArea5);
            legend5.Enabled = false;
            legend5.Name = "Legend1";
            this.Sobctv_Chart.Legends.Add(legend5);
            this.Sobctv_Chart.Location = new System.Drawing.Point(12, 317);
            this.Sobctv_Chart.Name = "Sobctv_Chart";
            series13.BorderWidth = 2;
            series13.ChartArea = "ChartArea1";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series13.Legend = "Legend1";
            series13.Name = "Series1";
            series13.YValuesPerPoint = 2;
            series14.BorderWidth = 2;
            series14.ChartArea = "ChartArea1";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series14.Legend = "Legend1";
            series14.Name = "Series2";
            series15.BorderWidth = 3;
            series15.ChartArea = "ChartArea1";
            series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series15.Legend = "Legend1";
            series15.Name = "Series3";
            this.Sobctv_Chart.Series.Add(series13);
            this.Sobctv_Chart.Series.Add(series14);
            this.Sobctv_Chart.Series.Add(series15);
            this.Sobctv_Chart.Size = new System.Drawing.Size(464, 250);
            this.Sobctv_Chart.TabIndex = 28;
            this.Sobctv_Chart.Text = "chart1";
            // 
            // SOB_VECTUR_CHART
            // 
            chartArea6.AxisX.Minimum = 0D;
            chartArea6.Name = "ChartArea1";
            this.SOB_VECTUR_CHART.ChartAreas.Add(chartArea6);
            legend6.Enabled = false;
            legend6.Name = "Legend1";
            this.SOB_VECTUR_CHART.Legends.Add(legend6);
            this.SOB_VECTUR_CHART.Location = new System.Drawing.Point(487, 317);
            this.SOB_VECTUR_CHART.Name = "SOB_VECTUR_CHART";
            series16.BorderWidth = 2;
            series16.ChartArea = "ChartArea1";
            series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series16.Legend = "Legend1";
            series16.Name = "Series1";
            series17.BorderWidth = 2;
            series17.ChartArea = "ChartArea1";
            series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series17.Legend = "Legend1";
            series17.Name = "Series2";
            series18.BorderWidth = 2;
            series18.ChartArea = "ChartArea1";
            series18.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series18.Legend = "Legend1";
            series18.Name = "Series3";
            this.SOB_VECTUR_CHART.Series.Add(series16);
            this.SOB_VECTUR_CHART.Series.Add(series17);
            this.SOB_VECTUR_CHART.Series.Add(series18);
            this.SOB_VECTUR_CHART.Size = new System.Drawing.Size(440, 250);
            this.SOB_VECTUR_CHART.TabIndex = 29;
            this.SOB_VECTUR_CHART.Text = "chart1";
            // 
            // NUMBER_OF_SOBCTV_VECTOR
            // 
            this.NUMBER_OF_SOBCTV_VECTOR.Location = new System.Drawing.Point(650, 291);
            this.NUMBER_OF_SOBCTV_VECTOR.Name = "NUMBER_OF_SOBCTV_VECTOR";
            this.NUMBER_OF_SOBCTV_VECTOR.Size = new System.Drawing.Size(100, 20);
            this.NUMBER_OF_SOBCTV_VECTOR.TabIndex = 30;
            this.NUMBER_OF_SOBCTV_VECTOR.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(160, 296);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(183, 17);
            this.label16.TabIndex = 31;
            this.label16.Text = "Собственные значения";
            // 
            // RESULT
            // 
            this.RESULT.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.RESULT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.RESULT.Location = new System.Drawing.Point(964, 239);
            this.RESULT.Multiline = true;
            this.RESULT.Name = "RESULT";
            this.RESULT.ReadOnly = true;
            this.RESULT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.RESULT.Size = new System.Drawing.Size(340, 219);
            this.RESULT.TabIndex = 32;
            // 
            // RESULT_BUTTON
            // 
            this.RESULT_BUTTON.Location = new System.Drawing.Point(1073, 546);
            this.RESULT_BUTTON.Name = "RESULT_BUTTON";
            this.RESULT_BUTTON.Size = new System.Drawing.Size(140, 40);
            this.RESULT_BUTTON.TabIndex = 33;
            this.RESULT_BUTTON.Text = "Результат";
            this.RESULT_BUTTON.UseVisualStyleBackColor = true;
            this.RESULT_BUTTON.Click += new System.EventHandler(this.RESULT_BUTTON_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(1006, 214);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(255, 15);
            this.label17.TabIndex = 34;
            this.label17.Text = "Информация о первой части задачи:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(626, 273);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(158, 15);
            this.label18.TabIndex = 35;
            this.label18.Text = "Собственные функции";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label19.Location = new System.Drawing.Point(1015, 513);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 15);
            this.label19.TabIndex = 36;
            this.label19.Text = "Невязки";
            // 
            // NEVYAZKI_TEXTBOX
            // 
            this.NEVYAZKI_TEXTBOX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.NEVYAZKI_TEXTBOX.Location = new System.Drawing.Point(1160, 512);
            this.NEVYAZKI_TEXTBOX.Name = "NEVYAZKI_TEXTBOX";
            this.NEVYAZKI_TEXTBOX.Size = new System.Drawing.Size(100, 21);
            this.NEVYAZKI_TEXTBOX.TabIndex = 37;
            this.NEVYAZKI_TEXTBOX.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label20.Location = new System.Drawing.Point(985, 476);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 15);
            this.label20.TabIndex = 38;
            this.label20.Text = "N";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label21.Location = new System.Drawing.Point(1061, 476);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(18, 15);
            this.label21.TabIndex = 39;
            this.label21.Text = "M";
            // 
            // M_text
            // 
            this.M_text.Location = new System.Drawing.Point(1088, 475);
            this.M_text.Name = "M_text";
            this.M_text.Size = new System.Drawing.Size(35, 20);
            this.M_text.TabIndex = 40;
            this.M_text.Text = "4";
            // 
            // N_text
            // 
            this.N_text.Location = new System.Drawing.Point(1007, 475);
            this.N_text.Name = "N_text";
            this.N_text.Size = new System.Drawing.Size(40, 20);
            this.N_text.TabIndex = 41;
            this.N_text.Text = "4";
            // 
            // CHAST_check
            // 
            this.CHAST_check.AutoSize = true;
            this.CHAST_check.Location = new System.Drawing.Point(1171, 475);
            this.CHAST_check.Name = "CHAST_check";
            this.CHAST_check.Size = new System.Drawing.Size(108, 17);
            this.CHAST_check.TabIndex = 42;
            this.CHAST_check.Text = "Частный случай";
            this.CHAST_check.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1321, 598);
            this.Controls.Add(this.CHAST_check);
            this.Controls.Add(this.N_text);
            this.Controls.Add(this.M_text);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.NEVYAZKI_TEXTBOX);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.RESULT_BUTTON);
            this.Controls.Add(this.RESULT);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.NUMBER_OF_SOBCTV_VECTOR);
            this.Controls.Add(this.SOB_VECTUR_CHART);
            this.Controls.Add(this.Sobctv_Chart);
            this.Controls.Add(this.checkBox_poligarm);
            this.Controls.Add(this.checkBox_exp_fading);
            this.Controls.Add(this.checkBox_gauss);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.POR_KOR);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.DRAW_SIGNALS);
            this.Controls.Add(this.ARRAY);
            this.Controls.Add(this.Signal_Chart);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Каруен-Лоев";
            this.Load += new System.EventHandler(this.DRAW_SIGNALS_Click);
            ((System.ComponentModel.ISupportInitialize)(this.Signal_Chart)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Sobctv_Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SOB_VECTUR_CHART)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart Signal_Chart;
        private System.Windows.Forms.TextBox ARRAY;
        private System.Windows.Forms.TextBox Number_Of_Polig_Signal;
        private System.Windows.Forms.TextBox A1;
        private System.Windows.Forms.TextBox F1;
        private System.Windows.Forms.TextBox P1;
        private System.Windows.Forms.TextBox A2;
        private System.Windows.Forms.TextBox F2;
        private System.Windows.Forms.TextBox P2;
        private System.Windows.Forms.TextBox A3;
        private System.Windows.Forms.TextBox F3;
        private System.Windows.Forms.TextBox P3;
        private System.Windows.Forms.Button DRAW_SIGNALS;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Number_Of_Gauss_Signal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox disp3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox center1;
        private System.Windows.Forms.TextBox disp2;
        private System.Windows.Forms.TextBox center2;
        private System.Windows.Forms.TextBox AG3;
        private System.Windows.Forms.TextBox AG1;
        private System.Windows.Forms.TextBox center3;
        private System.Windows.Forms.TextBox disp1;
        private System.Windows.Forms.TextBox AG2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Number_Of_Fading_Polig_Signal;
        private System.Windows.Forms.TextBox F2Fading;
        private System.Windows.Forms.TextBox F1Fading;
        private System.Windows.Forms.TextBox P3Fading;
        private System.Windows.Forms.TextBox P1Fading;
        private System.Windows.Forms.TextBox F3Fading;
        private System.Windows.Forms.TextBox P2Fading;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox FadingKoef3;
        private System.Windows.Forms.TextBox FadingKoef2;
        private System.Windows.Forms.TextBox FadingKoef1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox POR_KOR;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox checkBox_gauss;
        private System.Windows.Forms.CheckBox checkBox_exp_fading;
        private System.Windows.Forms.CheckBox checkBox_poligarm;
        private System.Windows.Forms.DataVisualization.Charting.Chart Sobctv_Chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart SOB_VECTUR_CHART;
        private System.Windows.Forms.TextBox NUMBER_OF_SOBCTV_VECTOR;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox RESULT;
        private System.Windows.Forms.Button RESULT_BUTTON;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox NEVYAZKI_TEXTBOX;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox M_text;
        private System.Windows.Forms.TextBox N_text;
        private System.Windows.Forms.CheckBox CHAST_check;
    }
}

